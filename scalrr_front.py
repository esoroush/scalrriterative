import json
import sys
import numpy

import Queue

import Tkinter as tk
from Tkinter import *

import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg,NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from matplotlib.colors import Colormap

import SciDB
import zscale

HOST = 'rio.cs.washington.edu' # The remote host
PORT = 50007              # The same port as used by the server
ZSCALE = True 
class Visualizer:
    ui_root = None # ui_root for all ui objects
    frame = None
    _canvas = None
    
    scidb_connector = None
    previous_result = None
    
    iterations = list()

    def __init__(self, root, host, port):
        self.ui_root = root
        self.host = host
        self.port = port
        
        self.init_UI()
    
    def init_UI(self):
        self.ui_root.title("SciDB Visualizer")
        
        self.frame=tk.Frame(self.ui_root)
        self.frame.pack()
        
        query_frame=tk.Frame(self.frame)
        query_frame.grid(row=0)
        
        tk.Label(query_frame, text='Enter Query:').pack(padx=10, pady=0, side=LEFT, fill=X)
        self.query_text_gui = tk.Text(query_frame, width=40, height=1, borderwidth=5, takefocus=1)
        self.query_text_gui.pack(padx=10, pady=0, side=LEFT, fill=X)
        
        iteration_frame = tk.Frame(query_frame)
        iteration_frame.pack()
        self.iterative_checked = IntVar()
        tk.Checkbutton(iteration_frame, text="Iterative Query", variable=self.iterative_checked, command=self.toggleCheck).grid(row=0, column=0)
        self.num_of_iterations = StringVar()
        self.iteration_num_entry = tk.Entry(iteration_frame, textvariable=self.num_of_iterations)
        self.iteration_num_entry.grid(row=0, column=1)
        self.iteration_num_entry.grid_remove()
        
        tk.Button(query_frame, text="Execute", command=self.execute_query).pack(padx=10, pady=0)
        
        self.graph_frame = tk.Frame(self.frame)
        self.graph_frame.grid(row=2)
        self.iteration_buttons = tk.Frame(self.frame)
        self.iteration_buttons.grid(row=3)
        
        #tk.Button(self.frame, text="Quit", command=self.frame.quit).grid(row=3, sticky=W, padx=10, pady=10)
        
        Grid.rowconfigure(self.frame,1,weight=5)
        Grid.columnconfigure(self.frame,0,weight=5)
        
        f = Figure()
        ax = f.add_subplot(111)
        self.update_graph(f)
        
    def toggleCheck(self):
        if self.iterative_checked.get():
            self.iteration_num_entry.grid()
        else:
            self.iteration_num_entry.grid_remove()

    def add_graph(self, scatterplot):
        self.iterations.append(scatterplot)
        
        tk.Button(self.iteration_buttons, text="Iteration " + str(len(self.iterations)), command=lambda: self.update_graph(scatterplot)).pack(side=LEFT, fill=X)
        self.update_graph(scatterplot)
        
    def make_image(self, result, xlabel='X Axis', ylabel='Y Axis', title='ScatterPlot', grid=False):
        img = result.get_img_array(self.previous_result)
        if ZSCALE == True:
		z_min, z_max = zscale.zscale(result.get_data_array(), nsamples=2000, contrast=0.25) 
	else:
		z_min, z_max = result.get_min_max(result.get_data_array())
        fig = plt.figure()
        #cm = plt.get_cmap('RdYlBu_r')
        #cm = plt.get_cmap('jet')
        cm = plt.cm.Greys_r
	cm.set_over('#FFFFFF', alpha=1.0)
        cm.set_under('#000000', alpha=0.0)
        graph = fig.add_subplot(111)

        image = graph.imshow(img, cmap=cm, vmin=z_min, vmax=z_max, origin='lower')
        image.set_extent([result.xmin, result.xmax, result.ymin, result.ymax])
        
        graph.set_xlabel(xlabel, fontsize=20)
        graph.set_ylabel(ylabel, fontsize=20)
        graph.set_title(title)
        graph.grid(grid)
        fig.colorbar(image)
        
        return fig
        
    def update_graph(self, figure):
        self.graph_frame.destroy()
        self.graph_frame = tk.Frame(self.frame)
        self.graph_frame.grid(row=1)
        figure.set_size_inches(18.5,10.5)
        self._canvas = FigureCanvasTkAgg(figure, master=self.graph_frame)
        self._canvas.get_tk_widget().pack()
        self._canvas.show()
    
    def execute_query(self):
        query_text  = self.query_text_gui.get(1.0, END)
        #FOR TESTING#################################################
        #query_text  = 'select * from ndvi'
        #############################################################
        if query_text  == None or query_text  == '' or query_text  == '\n':
            return
        
        self.scidb_connector = SciDB.SciDB_Connector(self.host, self.port)
        if self.iterative_checked.get():
            self.scidb_connector.execute_query(query_text, iterative=True, iterations=int(self.num_of_iterations.get()))
        else:
            self.scidb_connector.execute_query(query_text)
        
        self.ui_root.after(100, self.handle_result) # problem if button clicked many times....
        
    def handle_result(self):
        """
        Check every 100 ms if there is something new in the queue.
        Handle all the messages currently in the queue (if any).
        """
#        if not self.running:
            # This is the brutal stop of the system. You may want to do
            # some cleanup before actually shutting it down.
#            import sys
#            sys.exit(1)
        
        while self.scidb_connector.queue.qsize():
            try:
                query_json = self.scidb_connector.queue.get(0)
        
                result = SciDB.SciDB_Result(query_json)
                
                # Make figure
                scatter = self.make_image(result, title="Iteration " + str(len(self.iterations) + 1))
                
                self.previous_result = result
                
                self.add_graph(scatter)
                print >> sys.stderr, 'done...'
                
            except Queue.Empty:
                pass
            
        self.ui_root.after(100, self.handle_result)

if __name__ == '__main__':
    root = tk.Tk()
    root.geometry("850x950+300+300")
    app = Visualizer(root, HOST, PORT)
    root.mainloop()
    

    
