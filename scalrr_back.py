import random
import json
#import md5
import traceback
import scidb_server_interface as sdbi
import mysql_server_interface as mdbi

import threading
import select
import socket
import sys

HOST = '0.0.0.0'
PORT = 50007            # Arbitrary non-privileged port
PACKET_SIZE = 1024

#databases
SCIDB = 'scidb'
MYSQL = 'mysql'

DEFAULT_DB = SCIDB

def dbconnect():
    """Make sure we are connected to the database each request."""
    if DEFAULT_DB == SCIDB:
    	sdbi.scidbOpenConn()
    else:
    	mdbi.mysqlOpenConn()

def dbclose():
    """Closes the database again at the end of the request."""
    if DEFAULT_DB == SCIDB:
    	sdbi.scidbCloseConn()
    else:
    	mdbi.mysqlOpenConn()

def process_request(options,query):
    dbconnect()
    response = query_execute(query,options)
    dbclose()
    return json.dumps(response)


#options: {reduce_res_check:True/False}
def query_execute(query, options):
    scidbi_options = {'afl':False}

    saved_qpresults = sdbi.verifyQuery(query, scidbi_options)

    if options['reduce_res_check'] and (saved_qpresults['size'] > sdbi.D3_DATA_THRESHOLD):
        # reduce the output            
        scidbi_options = {'afl':False, 'reduce_res':True}
        srtoptions = {'afl':False, 'saved_qpresults':saved_qpresults}

        if 'predicate' in options:
            srtoptions['predicate'] = options['predicate']
			
        if 'reduce_type' in options:
            scidbi_options['reduce_options'] = setup_reduce_type(options['reduce_type'], srtoptions)
        else :
            scidbi_options['reduce_options'] = setup_reduce_type('AGGR', srtoptions)
    else:
        # dont reduce the output
        scidbi_options = {'afl':False,'reduce_res':False}
   
    if options['iterative'] == True:
	 scidbi_options['iterations'] = options['iterations'];
	 scidbi_options['iterative'] = options['iterative'] 
    queryresultobj = sdbi.executeQuery(query, scidbi_options)
    
    scidbi_options={'dimnames':saved_qpresults['dims']}
    
    queryresultarr = sdbi.getAllAttrArrFromQueryForJSON(queryresultobj[0],scidbi_options)
    queryresultarr['dimnames'] = saved_qpresults['dims'];
    queryresultarr['dimbases'] = saved_qpresults['dimbases'];
    queryresultarr['dimwidths'] = saved_qpresults['dimwidths'];

    return queryresultarr # note that I don't set reduce_res false. JS code will still handle it properly


#returns necessary options for reduce type
#options: {'afl':True/False, 'predicate':"boolean predicate",'probability':double,'chunkdims':[]}
#required options: afl, predicate (if filter specified)
#TODO: make these reduce types match the scidb interface api reduce types
def setup_reduce_type(reduce_type,options):
        saved_qpresults = options['saved_qpresults']
	returnoptions = {'qpresults':saved_qpresults,'afl':options['afl']}
	returnoptions['reduce_type'] = sdbi.RESTYPE[reduce_type]
	#if reduce_type == 'agg':
	if 'chunkdims' in options:
		returnoptions['chunkdims'] = chunkdims
		#returnoptions['reduce_type'] = sdbi.RESTYPE['AGGR']
	#elif reduce_type == 'sample':
	if 'probability' in options:
		returnoptions['probability'] = options['probability']
		#returnoptions['reduce_type'] = sdbi.RESTYPE['SAMPLE']
	#elif reduce_type == 'filter':
	if 'predicate' in options:
		returnoptions['predicate'] = options['predicate']
		#returnoptions['reduce_type'] = sdbi.RESTYPE['FILTER']
	#else: #unrecognized type
	#	raise Exception("Unrecognized reduce type passed to the server.")
	return returnoptions

class Server:
    def __init__(self):
        self.host = HOST
        self.port = PORT
        self.backlog = 5
        self.size = PACKET_SIZE
        self.server = None
        self.threads = []

    def open_socket(self):
        for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            af, socktype, proto, canonname, sa = res
            try:
                self.server = socket.socket(af, socktype, proto)
            except socket.error, msg:
                self.server = None
                continue
            try:
                self.server.bind(sa)
                self.server.listen(1)
            except socket.error, msg:
                self.server.close()
                self.server = None
                continue
            break
        if self.server is None:
            print 'could not open socket'
            sys.exit(1)

    def run(self):
        self.open_socket()
        input = [self.server,sys.stdin]
        running = 1
        while running:
            inputready,outputready,exceptready = select.select(input,[],[])

            for s in inputready:

                if s == self.server:
                    # handle the server socket
                    c = Client(self.server.accept())
                    c.start()
                    self.threads.append(c)

                elif s == sys.stdin:
                    # handle standard input
                    junk = sys.stdin.readline()
                    running = 0

        # close all threads

        self.server.close()
        for c in self.threads:
            c.join()

class Client(threading.Thread):
    def __init__(self,(client,address)):
        threading.Thread.__init__(self)
        self.client = client
        self.address = address
        self.size = PACKET_SIZE

    def run(self):
        print 'Connected by', self.address
        
        request = self.client.makefile().readline()
        print "Recv: ",request
        
        jsonRequest = json.loads(request) # parse string into json
    	if jsonRequest['function'] == "query_execute":
        	options = jsonRequest['options']
        	query = str(jsonRequest['query'])
    	else:
        	raise Exception("unrecognized function passed")
	if options['iterative']:
		numIter =  options['iterations']
		for index in xrange(1,numIter):
			### JUST FOR NOW #####
			query = query.replace("@","@"+str(index))
			response = process_request(options,query)
			
	        	print >> sys.stderr, "Sending: (length)", response
        		self.client.send(response)
	else:
        	response = process_request(options,query)
	        print >> sys.stderr, "Sending: (length)", response
        	self.client.send(response)
        
	self.client.close()

if __name__ == "__main__":
    s = Server()
    s.run() 
