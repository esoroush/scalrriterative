import socket
import json
import sys
import threading
import numpy
import Queue
#for testing
import test_data

class SciDB_Connector(threading.Thread):
    scidb_socket = None
    queue = None
    
    query = None
    
    def __init__(self, host, port):
        threading.Thread.__init__(self)
        self.scidb_socket = SciDB_Socket(host, port)
        self.queue = Queue.Queue()
    
    def build_query(self, query_text, reduce_check=True, reduce_type='AGGR', iterative=False, iterations=0):
        # build query
        options = {'reduce_res_check':reduce_check}
        
        if reduce_check:
            options['reduce_res'] = True
            options['reduce_type'] = reduce_type
           
        options['iterative'] = iterative 
        if iterative:
            options['iterations'] = iterations
        
        server_request = {'query':query_text,'options':options,'function':'query_execute'}
        
        return server_request;
        
    def execute_query(self, query_text, reduce_check=True, reduce_type='AGGR', iterative=False, iterations=0): 
        self.query  = self.build_query(query_text, reduce_check, reduce_type, iterative, iterations)
        print >> sys.stderr,self.query
        self.start()
        
    def run(self):
        #FOR TESTING#######################################
        #self.queue.put(test_data.test_json)
        #self.queue.put(test_data.test_json_2)
        #self.queue.put(test_data.test_json_3)
        #self.queue.put(test_data.test_json_4)
        #return
        ###################################################
        
        # connect to sci db
        self.scidb_socket.connect()
        
        # send the query
        self.scidb_socket.send(self.query)

        while 1:
            response = self.scidb_socket.get_json()
            
            if response: 
                self.queue.put(response)
            else:
                break
        
        # close connection to sci db
        self.scidb_socket.close()
        self.query = None
    
#    json_object = 
#    {
#    'dimbases': {
#       'xndvi': '1', 
#       'yndvi': '1'
#    }, 
#    'dimwidths': {'xndvi': 720, 'yndvi': 360}, 
#    'names': [
#        {'name': 'attrs.avg_ndvi', 'isattr': True}, 
#        {'name': 'dims.xndvi', 'isattr': False}, 
#        {'name': 'dims.yndvi', 'isattr': False}
#    ], 
#    'saved_qpresults': None, 
#    'types': {'attrs.avg_ndvi': 'double', 'dims.xndvi': 'int32', 'dims.yndvi': 'int32'}, 
#    'data': [
#        {'attrs.avg_ndvi': -0.2920943424369663, 'dims.xndvi': 65, 'dims.yndvi': 232}, 
#        {'attrs.avg_ndvi': -0.34001818983454407, 'dims.xndvi': 65, 'dims.yndvi': 233}, 
#        {'attrs.avg_ndvi': -0.38976100121132878, 'dims.xndvi': 65, 'dims.yndvi': 234}, 
#        {'attrs.avg_ndvi': -0.39701477965882137, 'dims.xndvi': 65, 'dims.yndvi': 235}, 
#        {'attrs.avg_ndvi': -0.33731062803603817, 'dims.xndvi': 65, 'dims.yndvi': 236}, 
#        {'attrs.avg_ndvi': -0.18660061074531836, 'dims.xndvi': 66, 'dims.yndvi': 232}, 
#        {'attrs.avg_ndvi': -0.3152613451400515, 'dims.xndvi': 66, 'dims.yndvi': 233}, 
#        {'attrs.avg_ndvi': -0.33239945592723374, 'dims.xndvi': 66, 'dims.yndvi': 234}, 
#        {'attrs.avg_ndvi': -0.39293752043865948, 'dims.xndvi': 66, 'dims.yndvi': 235}
#    ]
#   }
class SciDB_Result:
    data = None
    names = None
    types = None
    # name of each axis
    x_name = None
    y_name = None
    z_name = None
    # data of each axis
    img_array = None
    data_array = None
    
    x_offset = None
    y_offset = None
    
    
    def __init__(self, json_object):
        self.data = json_object['data']
        self.names = json_object['names']
        self.types = json_object['types']
        
        self.x_name = json_object['names'][2]['name']
        self.y_name = json_object['names'][1]['name']
        self.z_name = json_object['names'][0]['name']
        
    def get_img_array(self, master=None):
        if self.img_array is not None:
            return self.img_array
            
        if master is None:
            self.img_array = self.get_data_array()
        else:
            self.img_array = self.merge(master)
            
        for (x,y), value in numpy.ndenumerate(self.img_array):
            if self.img_array[x,y] is None:
                self.img_array[x,y] = -sys.maxint - 1

        return self.img_array
        
    def get_data_array(self):
        if self.data_array is not None:
            return self.data_array
            
        xmin, xmax = self._get_min_max(self.data, self.x_name)
        ymin, ymax = self._get_min_max(self.data, self.y_name)
        
        img = numpy.empty((xmax-xmin+1, ymax-ymin+1), order='C')
        img[:] = None
        
        for index in self.data:
            img[(index[self.x_name]-xmin, index[self.y_name]-ymin)] = index[self.z_name]
        
        self.data_array = img
        self.x_offset = xmin
        self.y_offset = ymin
        self.xmin = xmin
        self.xmax = xmax
        self.ymin= ymin
        self.ymax = ymax
        
        return self.data_array
        
    # change to support more data types
    def _get_min_max(self, data_dict, attr_name):
        ret_min = None
        ret_max = None
        
        for index in data_dict:
            value = index[attr_name]
            if ret_min is None:
                ret_min = value
            elif value < ret_min:
                ret_min = value
             
            if ret_max is None:
                ret_max = value
            elif value > ret_max:
                ret_max = value
        
        return ret_min, ret_max
        
    def get_min_max(self, array):
        ret_min = None
        ret_max = None
        
        for (x,y), value in numpy.ndenumerate(array):
            value = array[x,y]
            if numpy.isnan(value):
                continue
            
            if value is None:
                continue # skip this value
            
            if value == -sys.maxint -1:
                continue # skip this value
                
            if value == sys.maxint:
                continue # skip this value
            
            if ret_min is None:
                ret_min = value
            elif value < ret_min:
                ret_min = value
             
            if ret_max is None:
                ret_max = value
            elif value > ret_max:
                ret_max = value
        
        return ret_min, ret_max
    
    #Merges x,y,z provided into this data result returning x,y,z to graph
    #current aggr operator is addition
    def merge(self, result_master):
        delta = self.get_data_array()
        master = numpy.copy(result_master.get_data_array())
        img = numpy.copy(result_master.get_data_array())
        
        print >> sys.stderr, delta
        
        for (x,y), value in numpy.ndenumerate(delta):
            x_m = x + (result_master.x_offset - self.x_offset)
            y_m = y + (result_master.y_offset - self.y_offset)
            #master[x_m,y_m] = master[x_m,y_m] + delta[x,y] # TODO add more functions
            master[x_m,y_m] = delta[x,y] # HACK: for now we are sending the whole result from the backend
	    if abs(delta[x,y] - master[x_m,y_m]) < 0.001:
	    	img[x_m,y_m] = sys.maxint

        self.data_array = master
        self.x_offset = result_master.x_offset
        self.y_offset = result_master.y_offset
        self.xmin = result_master.xmin
        self.xmax = result_master.xmax
        self.ymin= result_master.ymin
        self.ymax = result_master.ymax
        
        return img

class SciDB_Socket():
    _host = None #hostname
    _port = None #port
    _socket = None #socket object
    
    def __init__(self, host, port):
        self._host = host
        self._port = port
        self._socket = None
        
    def connect(self):
        for res in socket.getaddrinfo(self._host, self._port, socket.AF_UNSPEC, socket.SOCK_STREAM):
            af, socktype, proto, canonname, sa = res
            
            # create socket
            try:
                self._socket = socket.socket(af, socktype, proto)
            except socket.error, msg:
                print msg
                self._socket = None
                continue
            
            # try to connect
            try:
                self._socket.connect(sa)
            except socket.error, msg:
                print msg
                self._socket.close()
                self._socket = None
                continue
            
            break
            
        if self._socket is None:
            print 'could not open socket'
            sys.exit(1)
        
        
    def close(self):
        """Make sure we close the connection"""
        if self._socket is None:
            return

        self._socket.close()
        self._socket = None
        
    def send(self, message):
        print >> sys.stderr, 'Sending:', message
        self._socket.send(json.dumps(message))
        self._socket.shutdown(socket.SHUT_WR)
        
    def get_json(self):
        try:
            response = self._socket.makefile().readline()
            print >> sys.stderr, 'Recv:', response
            return json.loads(response)
        except ValueError:
            return None
